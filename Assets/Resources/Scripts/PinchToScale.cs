using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PinchToScale : MonoBehaviour
{
    public float minScale = 1f, maxScale = 3.6f;
    private float _initialDistance;
    private Vector3 _initialScale;

    void Update()
    {
        if (Input.touchCount == 2)
        {
            // Deteksi 2 input sentuhan

            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);

            //  Jika salah satu tidak terdeteksi, stop

            if (touchZero.phase == TouchPhase.Ended || touchZero.phase == TouchPhase.Canceled
            || touchOne.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Canceled)
            {
                return;
            }

            // Jika terdeteksi dua sentuhan, ambil ukuran
            // awal skala model dan jarak antar sentuhan

            if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
            {
                _initialScale = this.transform.localScale;
                _initialDistance = Vector2.Distance(touchZero.position, touchOne.position);
            }
            else
            {
                // Jika sentuhan terjadi drag, ambil jarak
                // antar sentuhan "sekarang"

                var _currentDistance = Vector2.Distance(touchZero.position, touchOne.position);

                // Jika jarak antara sentuhan mendekati nol, return

                if (Mathf.Approximately(_initialDistance, 0)) return;
                var _factor = _currentDistance / _initialDistance;

                // Batas maksimum dan minimum perbesaran model

                if (this.transform.localScale.x <= maxScale && this.transform.localScale.x >= minScale)
                {
                    this.transform.localScale = _initialScale * _factor;
                    if (this.transform.localScale.x > maxScale)
                    {
                        this.transform.localScale = new Vector3(maxScale, maxScale, maxScale);
                    }
                    else if (this.transform.localScale.x < minScale)
                    {
                        this.transform.localScale = new Vector3(minScale, minScale, minScale);
                    }
                }

                // Debug.Log("Factor:" + _factor);
                // Debug.Log("Initial scale: " + _initialScale);
                // Debug.Log("Post scale: " + this.transform.localScale);
            }
        }
    }
}
