using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModelDisplayController : MonoBehaviour
{
    public GameObject[] modelArrays;
    public GameObject[] modelNameArrays;

    private void Awake()
    {
        Debug.Log("Awake!");
        string _imageTargetName = GameManager.Instance.TargetName;
        ChooseModelShown(_imageTargetName);
        ShowModelName(_imageTargetName);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) PreviousScene();
    }

    private void ChooseModelShown(string checkTargetName) {
        for (int i = 0; i < modelArrays.Length; i++)
        {
            if (checkTargetName == modelArrays[i].name)
            {
                modelArrays[i].SetActive(true);
            }
            else modelArrays[i].SetActive(false);
        }
        
    }

    private void ShowModelName(string checkTargetName) {
        Debug.Log("Compare Model Name to Target Name");
        for (int i = 0; i < modelNameArrays.Length; i++)
        {
            if (checkTargetName == modelNameArrays[i].name)
            {
                Debug.Log("Model Found");
                modelNameArrays[i].SetActive(true);
                if (modelNameArrays[i].activeInHierarchy)
                {
                    Debug.Log("Object shown!");
                }
            }
            else modelNameArrays[i].SetActive(false);
        }
    }

    public void PreviousScene()
    {
        SceneManager.LoadScene("EncyclopediaScene");
    }

    public void ToArScene()
    {
        SceneManager.LoadScene("AugmentedRealityScene");
    }
}
