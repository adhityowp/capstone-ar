using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TataSuryaObjectManager : MonoBehaviour
{
    int objectsUnlocked;
    public Button[] buttons;
    public Text[] texts;
    public Text[] visibleTexts;
    public Text myScore;

    void Update()
    {
        myScore.text = "Skorku: " + PlayerPrefs.GetInt(GlobalConstants.KeyScoreValue, 0).ToString();
        objectsUnlocked = 7;
        for (int i = 0; i < texts.Length; i++)
        {
            visibleTexts[i].gameObject.SetActive(i + 1 <= objectsUnlocked);
            buttons[i].interactable = (i + 1 <= objectsUnlocked);
            texts[i].gameObject.SetActive(i + 1 > objectsUnlocked);
        }
    }

    public void LoadEncyScene(string objectName)
    {
        ArrayList visitedPoints = PlayerSession.LoadVisitedPoints();
        if (!visitedPoints.Contains(objectName))
        {
            PlayerSession.UpdateLearingProgress(PlayerSession.GetLearningProgress() + 1);
            visitedPoints.Add(objectName);
            PlayerSession.UpdateVisitedPoints(visitedPoints);
        }
        GameManager.Instance.TargetName = objectName;
        GameManager.Instance.ParentScene = "TataSuryaObjectsScene";
        SceneManager.LoadScene("EncyclopediaScene");
    }

    public void loadQuizScene()
    {
        SceneManager.LoadScene("TataSuryaQuizScene");
    }
}
