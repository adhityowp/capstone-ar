using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Home_UIController : MonoBehaviour
{
    public void openARScene()
    {
        SceneManager.LoadScene("AugmentedRealityScene");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit ();
    }
}
