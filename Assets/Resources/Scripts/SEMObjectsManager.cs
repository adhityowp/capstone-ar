using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SEMObjectsManager : MonoBehaviour
{
    int objectsUnlocked;
    public Button[] buttons;
    public Text[] texts, visibleTexts;
    public Text myScore;

    void Start()
    {
        myScore.text = "Skorku: " + PlayerPrefs.GetInt(GlobalConstants.KeyScoreValue, 0).ToString();
        objectsUnlocked = 6;
        for (int i = 0; i < texts.Length; i++)
        {
            visibleTexts[i].gameObject.SetActive(i + 1 <= objectsUnlocked);
            buttons[i].interactable = (i + 1 <= objectsUnlocked);
            texts[i].gameObject.SetActive(i + 1 > objectsUnlocked);
        }
    }

    public void loadEncyScene(string objectName)
    {
        ArrayList visitedPoints = PlayerSession.LoadVisitedPoints();
        if (!visitedPoints.Contains(objectName))
        {
            PlayerSession.UpdateLearingProgress(PlayerSession.GetLearningProgress() + 1);
            visitedPoints.Add(objectName);
            PlayerSession.UpdateVisitedPoints(visitedPoints);
        }
        GameManager.Instance.TargetName = objectName;
        GameManager.Instance.ParentScene = "SEMObjectsScene";
        SceneManager.LoadScene("EncyclopediaScene");
    }

    public void loadQuizScene()
    {
        SceneManager.LoadScene("SEMQuizScene");
    }
}
