using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WarningPanelManager : MonoBehaviour
{
    public Button[] buttons;
    public Text[] texts, visibleTexts;
    
    
    void Start()
    {
        int markersUnlocked = PlayerSession.GetLevelsUnlocked();
        for (int i = 0; i < texts.Length; i++)
        {
            if (i + 1 <= markersUnlocked)
            {
                SetLevelsVisibility(i, true);
            }
            else SetLevelsVisibility(i, false);
        }
    }

    public void SetLevelsVisibility(int index, bool isVisible)
    {
        visibleTexts[index].gameObject.SetActive(isVisible);
        buttons[index].interactable = isVisible;
        texts[index].gameObject.SetActive(!isVisible);
    }

    public void DownloadMarker(string url)
    {
        Application.OpenURL(url);
    }

    public void LoadArScene()
    {
        GameManager.Instance.ParentScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene("AugmentedRealityScene");
    }
}
