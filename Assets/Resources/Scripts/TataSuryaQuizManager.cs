using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TataSuryaQuizManager : MonoBehaviour
{
    public const int totalTataSuryaMCQuestions = 10;
    public const int eachQuestionScore = (100 / totalTataSuryaMCQuestions);
    static int currentQuestion, currentScore, passedQuestions;
    string[] keyAnswers = { "Option 2", "Option 3", "Option 1", "Option 1", "Option 1", "Option 1", "Option 1", "Option 2", "Option 3" };
    public string word;
    string str = "";
    static bool isVerified;
    public Button verifyButton, nextButton, prevButton, finishedButton;
    public Text DDcorrectText, DDwrongText, MCcorrectText, MCwrongText, scoreText, questionNumber;
    public GameObject[] questions, letters, letterShadows, boxes;
    public ToggleGroup[] toggleGroups;

    void Start()
    {
        isVerified = false;
        if (passedQuestions < totalTataSuryaMCQuestions)
        {
            currentQuestion = passedQuestions + 1;
        }
        else currentQuestion = passedQuestions;
        currentScore = PlayerPrefs.GetInt(GlobalConstants.KeyScoreValue, 0);
        setLearningProgress();
    }

    private void Update()
    {
        passedQuestions = PlayerPrefs.GetInt(GlobalConstants.KeyPassedTataSuryaMCQuestions, 0);
        updateQuestionVisibility();
        updateButtonsVisibility();
        updateScreenOrientation();
        scoreText.text = "Skorku: " + currentScore.ToString();
        questionNumber.text = "Kuis Tata Surya: " + currentQuestion.ToString();
    }

    public void nextQuestion()
    {
        currentQuestion++;
        isVerified = false;
        MCcorrectText.gameObject.SetActive(false);
        MCwrongText.gameObject.SetActive(false);
        DDcorrectText.gameObject.SetActive(false);
        DDwrongText.gameObject.SetActive(false);
        Reload();
        setScoreColor(false);
    }

    public void prevQuestion()
    {
        currentQuestion--;
        isVerified = false;
        MCcorrectText.gameObject.SetActive(false);
        MCwrongText.gameObject.SetActive(false);
        DDcorrectText.gameObject.SetActive(false);
        DDwrongText.gameObject.SetActive(false);
        Reload();
        setScoreColor(false);
    }

    public void verifyQuestion()
    {
        isVerified = true;
        if (currentQuestion == totalTataSuryaMCQuestions)
        {
            VerifyDragDropQuestion();
        }
        else VerifyMultipleChoiceQuestion();
        if (currentQuestion > passedQuestions) PlayerPrefs.SetInt(GlobalConstants.KeyPassedTataSuryaMCQuestions, currentQuestion);
    }

    private void VerifyMultipleChoiceQuestion()
    {
        string chosenAnswer = toggleGroups[currentQuestion - 1].ActiveToggles().FirstOrDefault().name;
        MCwrongText.gameObject.SetActive(chosenAnswer != keyAnswers[currentQuestion - 1]);
        if (chosenAnswer == keyAnswers[currentQuestion - 1]) onCorrectAnswer();
    }

    private void VerifyDragDropQuestion()
    {
        for (int i = 0; i < letters.Length; i++)
        {
            str += boxes[i].name;
        }
        if (word == str)
        {
            onCorrectAnswer();
        } else DDwrongText.gameObject.SetActive(true);
    }

    private void onCorrectAnswer()
    {
        if(currentQuestion == totalTataSuryaMCQuestions)
        {
            DDcorrectText.gameObject.SetActive(true);
        } else MCcorrectText.gameObject.SetActive(true);
        if (currentQuestion > passedQuestions)
        {
            setScoreColor(true);
            setNewScore(eachQuestionScore);
        }
    }

    private void updateButtonsVisibility()
    {
        verifyButton.gameObject.SetActive(!isVerified);
        prevButton.gameObject.SetActive(currentQuestion > 1);
        nextButton.gameObject.SetActive(isVerified && (currentQuestion < totalTataSuryaMCQuestions));
        finishedButton.gameObject.SetActive(isVerified && (currentQuestion == totalTataSuryaMCQuestions));
    }

    public void updateQuestionVisibility()
    {
        for (int i = 0; i < totalTataSuryaMCQuestions; i++)
        {
            questions[i].SetActive(i == (currentQuestion - 1));
        }
    }

    public void updateScreenOrientation()
    {
        if(currentQuestion == totalTataSuryaMCQuestions)
        {
            Screen.orientation = ScreenOrientation.Landscape;
        } else Screen.orientation = ScreenOrientation.Portrait;
    }

    public void setScoreColor(bool isNew)
    {
        if (isNew)
        {
            Color green;
            ColorUtility.TryParseHtmlString("#00FF44", out green);
            scoreText.color = green;
        } else
        {
            Color white;
            ColorUtility.TryParseHtmlString("#F2F2F2", out white);
            scoreText.color = white;
        }
    }

    public void setNewScore(int newValue)
    {
        currentScore += newValue;
        setScoreColor(true);
        PlayerPrefs.SetInt(GlobalConstants.KeyScoreValue, currentScore);
    }

    public void setLearningProgress()
    {
        ArrayList visitedPoints = PlayerSession.LoadVisitedPoints();
        string currentScene = SceneManager.GetActiveScene().name;
        if (!visitedPoints.Contains(currentScene))
        {
            PlayerSession.UpdateLearingProgress(PlayerSession.GetLearningProgress() + 1);
            visitedPoints.Add(currentScene);
            PlayerSession.UpdateVisitedPoints(visitedPoints);
        }
    }

    public void DragLetter(int index)
    {
        letters[index].transform.position = Input.mousePosition;
    }

    public void DropLetter(int index)
    {
        int attachedIndex = -1;
        for (int i = 0; i < letters.Length; i++)
        {
            float differences = Vector3.Distance(letters[index].transform.position, boxes[i].transform.position);
            if (differences < 50)
            {
                attachedIndex = i;
            }
        }

        if (attachedIndex >= 0)
        {

            letters[index].transform.localScale = boxes[attachedIndex].transform.localScale;
            letters[index].transform.position = boxes[attachedIndex].transform.position;
            boxes[attachedIndex].name = letters[index].name;
        }
        else
        {
            letters[index].transform.position = letterShadows[index].transform.position;
        }
    }

    public void Reload()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        str = "";
        for (int i = 0; i < letters.Length; i++)
        {
            boxes[i].name = (i + 1).ToString();
            letters[i].transform.position = letterShadows[i].transform.position;
            letters[i].transform.localScale = letterShadows[i].transform.localScale;
        }
    }
}