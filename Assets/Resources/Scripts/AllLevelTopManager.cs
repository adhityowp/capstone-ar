using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AllLevelTopManager : MonoBehaviour
{
    static int progressValue;
    static int scoreValue;
    public Text valueText;
    public Text myScoreText;
    public Slider slider;

    private void Start()
    {
        slider.interactable = false;
        progressValue = PlayerSession.GetLearningProgress();
        scoreValue = PlayerPrefs.GetInt(GlobalConstants.KeyScoreValue, 0);
        slider.value = progressValue;
        myScoreText.text = $"Skorku: {scoreValue}";
        valueText.text = "Progres belajarku: " + GetValuePresentage(progressValue).ToString() + "%";
    }

    private float GetValuePresentage(float value)
    {
        if (value > 0)
        {
            return value * 100 / PlayerSession.maxProgress;
        } else return 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Close();
    }

    public void LoadAchievements()
    {
        GameManager.Instance.ParentScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene("AchievementsScene");
    }

    public void Close()
    {
        Application.Quit();
    }
}
