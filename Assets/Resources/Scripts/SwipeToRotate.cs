using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeToRotate : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Transform target;
    [SerializeField] private float distanceToTarget, rotationSpeed, limitAngle = 30;
    private Vector3 previousPosition, direction;
    private float _alreadyRotatedAxis;

    private void Update()
    {
        if(Input.touchCount == 1)
        {
            Touch touchZero = Input.GetTouch(0);

            switch (touchZero.phase)
            {
                case TouchPhase.Began :
                    Debug.Log("Began");
                    previousPosition = touchZero.position;
                    break;

                case TouchPhase.Moved :
                    Debug.Log("Moved");
                    direction = touchZero.deltaPosition;
                    Debug.Log(direction);
                    break;

                default :
                    Debug.Log("Ended");
                    break;
            }
            
            if (Mathf.Approximately(direction.x, 1)) return;
            if (Mathf.Approximately(direction.y, 1)) return;

            float _rotationY = -direction.y * rotationSpeed * Mathf.Deg2Rad;
            float _rotationX = direction.x * rotationSpeed * Mathf.Deg2Rad;
            
            // Tilt down
            if (_rotationY < 0)
            {
                // Cek nilai yang lebih besar dari sudut pergerakan mouse/touch
                // atau nilai sudut yang sudah diputar (_alreadyRotatedAxis). 
                // Jika nilai sudut yang sudah diputar == nilai batas sudut (limitAngle), 
                // maka objek tidak dapat di-tilt lagi
                _rotationY = Mathf.Max(_rotationY, -limitAngle - _alreadyRotatedAxis);
            }
            
            // Tilt up
            else if (_rotationY > 0)
            {
                // Cek nilai yang lebih kecil dari sudut pergerakan mouse/touch
                // atau nilai sudut yang sudah diputar (_alreadyRotatedAxis). 
                // Jika nilai sudut yang sudah diputar == nilai batas sudut (limitAngle), 
                // maka objek tidak dapat di-tilt lagi
                _rotationY = Mathf.Min(_rotationY, limitAngle - _alreadyRotatedAxis);
            }

            cam.transform.position = target.position;

            transform.Rotate(Vector3.up, -_rotationX);
            cam.transform.Rotate(Vector3.right, _rotationY, Space.World);
            
            cam.transform.Translate(new Vector3(0, 0, -distanceToTarget));

            // Update nilai sudut yang sudah diputar
            _alreadyRotatedAxis += _rotationY;
        }
    }
}
