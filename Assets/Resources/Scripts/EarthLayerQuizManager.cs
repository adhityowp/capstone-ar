using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EarthLayerQuizManager : MonoBehaviour
{

    public const int totalEarthLayerMCQuestions = 9;
    public const int eachQuestionScore = (100 / totalEarthLayerMCQuestions);
    static int currentQuestion, currentScore, passedQuestions;
    string[] keyAnswers = { "Option 1", "Option 4", "Option 1", "Option 1", "Option 2", "Option 1", "Option 1", "Option 3", "Option 3"};
    static bool isVerified;
    public Button verifyButton, nextButton, prevButton, finishedButton;
    public Text MCcorrectText, MCwrongText, scoreText, questionNumber;
    public GameObject[] questions;
    public ToggleGroup[] toggleGroups;

    void Start()
    {
        isVerified = false;
        if (passedQuestions < totalEarthLayerMCQuestions)
        {
            currentQuestion = passedQuestions + 1;
        }
        else currentQuestion = passedQuestions;
        currentScore = PlayerPrefs.GetInt(GlobalConstants.KeyScoreValue, 0);
        setLearningProgress();
    }

    private void Update()
    {
        passedQuestions = PlayerPrefs.GetInt(GlobalConstants.KeyPassedEarthLayerQuestions, 0);
        updateQuestionVisibility();
        updateButtonsVisibility();
        scoreText.text = "Skorku: " + currentScore.ToString();
        questionNumber.text = "Kuis Lapisan Bumi: " + currentQuestion.ToString();

        if (Input.GetKeyDown(KeyCode.Escape)) Close();
    }

    public void nextQuestion()
    {
        currentQuestion++;
        isVerified = false;
        MCcorrectText.gameObject.SetActive(false);
        MCwrongText.gameObject.SetActive(false);
        setScoreColor(false);
    }

    public void prevQuestion()
    {
        currentQuestion--;
        isVerified = false;
        MCcorrectText.gameObject.SetActive(false);
        MCwrongText.gameObject.SetActive(false);
        setScoreColor(false);
    }

    public void verifyQuestion()
    {
        isVerified = true;
        string chosenAnswer = toggleGroups[currentQuestion - 1].ActiveToggles().FirstOrDefault().name;
        MCwrongText.gameObject.SetActive(chosenAnswer != keyAnswers[currentQuestion - 1]);
        if (chosenAnswer == keyAnswers[currentQuestion - 1]) onCorrectAnswer();
        if (currentQuestion > passedQuestions) PlayerPrefs.SetInt(GlobalConstants.KeyPassedEarthLayerQuestions, currentQuestion);
    }

    private void onCorrectAnswer()
    {
        MCcorrectText.gameObject.SetActive(true);
        if (currentQuestion > passedQuestions)
        {
            setScoreColor(true);
            setNewScore(eachQuestionScore);
        }
    }

    private void updateButtonsVisibility()
    {
        verifyButton.gameObject.SetActive(!isVerified);
        prevButton.gameObject.SetActive(currentQuestion > 1);
        nextButton.gameObject.SetActive(isVerified && (currentQuestion < totalEarthLayerMCQuestions));
        finishedButton.gameObject.SetActive(isVerified && (currentQuestion == totalEarthLayerMCQuestions));
    }

    public void updateQuestionVisibility()
    {
        for (int i = 0; i < totalEarthLayerMCQuestions; i++)
        {
            questions[i].SetActive(i == (currentQuestion - 1));
        }
    }

    public void setScoreColor(bool isNew)
    {
        if (isNew)
        {
            Color green;
            ColorUtility.TryParseHtmlString("#00FF44", out green);
            scoreText.color = green;
        }
        else
        {
            Color white;
            ColorUtility.TryParseHtmlString("#F2F2F2", out white);
            scoreText.color = white;
        }
    }

    public void setNewScore(int newValue)
    {
        currentScore += newValue;
        setScoreColor(true);
        PlayerPrefs.SetInt(GlobalConstants.KeyScoreValue, currentScore);
    }

    public void setLearningProgress()
    {
        ArrayList visitedPoints = PlayerSession.LoadVisitedPoints();
        string currentScene = SceneManager.GetActiveScene().name;
        if (!visitedPoints.Contains(currentScene))
        {
            PlayerSession.UpdateLearingProgress(PlayerSession.GetLearningProgress() + 1);
            visitedPoints.Add(currentScene);
            PlayerSession.UpdateVisitedPoints(visitedPoints);
        }
    }

    public void Close()
    {
        SceneManager.LoadScene("EarthLayerObjectsScene");
    }
}