using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EachLevel_UIController : MonoBehaviour
{
    void Start()
    {
        ArrayList visitedPoints = PlayerSession.LoadVisitedPoints();
        string currentScene = SceneManager.GetActiveScene().name;
        if (!visitedPoints.Contains(currentScene))
        {

            PlayerSession.UpdateLearingProgress(PlayerSession.GetLearningProgress() + 1);
            visitedPoints.Add(currentScene);
            PlayerSession.UpdateVisitedPoints(visitedPoints);
        }
    }
    void Update()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        if (Input.GetKeyDown(KeyCode.Escape)) Close();
    }

    public void Close()
    {
        SceneManager.LoadScene("AllLevelsScene");
    }
}