using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ency_UIController : MonoBehaviour
{
    public GameObject[] encyPanels;
    public GameObject defaultPanel;
    static string _imageTargetName;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) PreviousScene();
    }

    private void Awake()
    {
        // Memanggil targetName yang ada di GameManager
        // dan menaruh di string lokal yang kemudian
        // dijadikan parameter di method "ChoosePanelShown"

        _imageTargetName = GameManager.Instance.TargetName;
        ChoosePanelShown(_imageTargetName);
    }

    public void ChangeSceneToModelViewer()
    {
        ArrayList visitedPoints = PlayerSession.LoadVisitedPoints();
        string modelName = _imageTargetName + "_model";
        if (!visitedPoints.Contains(modelName))
        {
            PlayerSession.UpdateLearingProgress(PlayerSession.GetLearningProgress() + 1);
            visitedPoints.Add(modelName);
            PlayerSession.UpdateVisitedPoints(visitedPoints);
        }
        SceneManager.LoadScene("ModelViewerScene");
    }

    private void ChoosePanelShown(string checkTargetName)
    {
        // Mengaktivasi elemen UI (panel) yang
        // sesuai dengan target yang terlacak.

        for (int i = 0; i < encyPanels.Length; i++)
        {
            if (checkTargetName == encyPanels[i].name)
            {
                defaultPanel.SetActive(false);
                encyPanels[i].SetActive(true);
                break;
            }
            encyPanels[i].SetActive(false);
        }
    }

    public void PreviousScene()
    {
        SceneManager.LoadScene(GameManager.Instance.ParentScene);
    }
}
