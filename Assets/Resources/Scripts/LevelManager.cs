using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class LevelManager : MonoBehaviour
{
    public Button[] buttons;
    public Text[] texts, visibleTexts;
    public GameObject warningPanel, levelManager;
   

    void Start()
    {
        openWarningPanel(false);
        int levelsUnlocked = PlayerSession.GetLevelsUnlocked();
        for (int i = 0; i < texts.Length; i++)
        {
            if (i + 1 <= levelsUnlocked)
            {
                SetLevelsVisibility(i, true);
            }
            else SetLevelsVisibility(i, false);
        }
    }

    public void SetLevelsVisibility(int index, bool isVisible)
    {
        visibleTexts[index].gameObject.SetActive(isVisible);
        buttons[index].interactable = isVisible;
        texts[index].gameObject.SetActive(!isVisible);
    }

    public void LoadLevel(string sceneName)
    {
        GameManager.Instance.ParentScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }

    public void openWarningPanel(bool isOpen = true)
    {
        levelManager.SetActive(!isOpen);
        warningPanel.SetActive(isOpen);
    }
}
