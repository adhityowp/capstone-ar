using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public string TargetName { get; set; }
    public string ParentScene { get; set; }
    private int _sceneIndex;

    // Singleton GameManager sehingga variable yang di-set
    // di-script ini dapat dipanggil di semua scene.

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance is null)
            {
                Debug.LogError("Game Manager is Null!");
                if (_instance is null)
                {
                    GameObject go = new GameObject
                    {
                        name = "GameManager"
                    };
                    _instance = go.AddComponent<GameManager>();
                    Debug.LogError("Game Manager created!");
                    DontDestroyOnLoad(go);
                }
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
