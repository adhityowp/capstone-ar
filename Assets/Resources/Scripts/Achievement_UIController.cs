using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Achievement_UIController : MonoBehaviour
{
    public Image[] enabledThropies, disabledThropies;

    void Start()
    {
        int scoreValue = PlayerPrefs.GetInt(GlobalConstants.KeyScoreValue, 0);

        for (int i = 0; i < enabledThropies.Length; i++)
        {
            if (i + 1 <= GetThropiesEnabilityTotal(scoreValue))
            {
                SetThropiesVisibility(i, true);
            } else SetThropiesVisibility(i, false);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Close();
    }

    private int GetThropiesEnabilityTotal(int scoreValue)
    {
        if (scoreValue >= 400)
        {
            return 4;
        }
        else if (scoreValue >= 300)
        {
            return 3;
        }
        else if (scoreValue >= 200)
        {
            return 2;
        }
        else if (scoreValue >= 100)
        {
            return 1;
        }
        else return 0;
    }

    private void SetThropiesVisibility(int index, bool isVisible)
    {
        enabledThropies[index].gameObject.SetActive(isVisible);
        disabledThropies[index].gameObject.SetActive(!isVisible);
    }

    public void Close()
    {
        SceneManager.LoadScene(GameManager.Instance.ParentScene);
    }
}