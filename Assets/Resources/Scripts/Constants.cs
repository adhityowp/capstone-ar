﻿public static class GlobalConstants
{
    public const string KeyUnlockedAnggotaTataSuryaObjects = "key_unlosdfcked_anggota_tata_surya_objects";
    public const string KeyUnlockedTataSuryaObjects = "key_unlocked_tata_surya_objects";
    public const string KeyProgressValue = "key_progress_value";
    public const string KeyScoreValue = "key_score_value";
    public const string KeyPassedTataSuryaMCQuestions = "key_passed_ts_mc_questions";
    public const string KeyPassedAnggotaTataSuryaMCQuestions = "key_passed_ats_mc_questions";
    public const string KeyPassedSEMMCQuestions = "key_passed_sem_mc_questions";
    public const string KeyPassedEarthLayerQuestions = "key_passed_sem_mc_questions";
    public const string KeyUserCheckpoints = "key_user_checkpoints";
}