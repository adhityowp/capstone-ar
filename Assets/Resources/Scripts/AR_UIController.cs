using UnityEngine;
using Vuforia;
using UnityEngine.SceneManagement;

public class AR_UIController : MonoBehaviour
{
    public GameObject[] infoPanels;
    public GameObject defaultPanel;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) SceneManager.LoadScene(GameManager.Instance.ParentScene);
    }

    public void GetTargetName(ImageTargetBehaviour imageTargetBehaviour)
    {
        // Mengambil atribut nama dari target yang terlacak.

        string imageTargetName = imageTargetBehaviour.TargetName;

        // Cek status tracking:
        // imageTargetBehaviour.TargetStatus.Status == Status. ...
        // imageTargetBehaviour.TargetStatus.StatusInfo == StatusInfo.NORMAL

        if (imageTargetName != null)
        {
            HidePanels();
            ShowPanel(imageTargetName);

            // Memasukan nama target yang terlacak ke
            // variable singleton GameManager, sehingga
            // dapat dengan mudah digunakan di berbagai scene.

            GameManager.Instance.TargetName = imageTargetName;
        }
        else
        {
            Debug.LogError("imageTargetName is Null!");
        }

    }

    public void HidePanels()
    {
        // Mematikan (deactivate) semua GameObject
        // "infoPanels" yang ada di-scene.

        for (int i = 0; i < infoPanels.Length; i++)
        {
            infoPanels[i].SetActive(false);
        }
        //Debug.Log("Panels cleared!");
        defaultPanel.SetActive(true);
    }

    void ShowPanel(string checkTargetName)
    {
        // Mengaktivasi elemen UI (panel) yang
        // sesuai dengan target yang terlacak.
        
        for (int i = 0; i < infoPanels.Length; i++) {
            // Debug.Log("Nama Target: " + checkTargetName);
            // Debug.Log("Nama Panel: " + infoPanels[i].name);
            if (checkTargetName == infoPanels[i].name)
            {
                // Debug.Log("Correct Panel");
                defaultPanel.SetActive(false);
                infoPanels[i].SetActive(true);
                break;
            }
            else
            {
                Debug.LogError("WARNING! Mismatch target name with panel name!");
                defaultPanel.SetActive(true);
            }
            infoPanels[i].SetActive(false);
        }
    }

    public void NextScene()
    {
        SceneManager.LoadScene("EncyclopediaScene");
        // Debug.Log("Next Scene");
    }

}
