using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AnggotaTataSuryaObjectsManager : MonoBehaviour
{
    int objectsUnlocked;
    public Button[] buttons;
    public Text[] texts, visibleTexts;
    public Text myScore;

    void Update()
    {
        myScore.text = "Skorku: " + PlayerPrefs.GetInt(GlobalConstants.KeyScoreValue, 0).ToString();
        objectsUnlocked = 8;
        for (int i = 0; i < texts.Length; i++)
        {
            visibleTexts[i].gameObject.SetActive(i + 1 <= objectsUnlocked);
            buttons[i].interactable = (i + 1 <= objectsUnlocked);
            texts[i].gameObject.SetActive(i + 1 > objectsUnlocked);
        }
    }

    public void loadEncyScene(string objectName)
    {
        ArrayList visitedPoints = PlayerSession.LoadVisitedPoints();
        if (!visitedPoints.Contains(objectName))
        {
            PlayerSession.UpdateLearingProgress(PlayerSession.GetLearningProgress() + 1);
            visitedPoints.Add(objectName);
            PlayerSession.UpdateVisitedPoints(visitedPoints);
        }
        GameManager.Instance.TargetName = objectName;
        GameManager.Instance.ParentScene = "AnggotaTataSuryaObjectsScene";
        SceneManager.LoadScene("EncyclopediaScene");
    }

    public void loadQuizScene()
    {
        SceneManager.LoadScene("AnggotaTataSuryaQuizScene");
    }
}
