using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class PlayerSession
{
    public const int maxProgress = 52;

    public static void UpdateLearingProgress(int newValue)
    {
        PlayerPrefs.SetInt(GlobalConstants.KeyProgressValue, newValue);
    }

    public static int GetLearningProgress()
    {
        return PlayerPrefs.GetInt(GlobalConstants.KeyProgressValue, 0);
    }

    public static void UpdateVisitedPoints(ArrayList visitedPoints)
    {
        var strings = visitedPoints.Cast<string>().ToArray();
        string saveString = string.Join(",", strings);
        Debug.Log("update " + saveString);
        PlayerPrefs.SetString(GlobalConstants.KeyUserCheckpoints, saveString);
    }

    public static ArrayList LoadVisitedPoints()
    {
        string loadString = PlayerPrefs.GetString(GlobalConstants.KeyUserCheckpoints, "");
        Debug.Log("blom " + loadString);
        if (loadString == "")
        {
            Debug.Log("load if " + loadString);
            return new ArrayList() { };
        } else
        {
            Debug.Log("load else " + loadString);
            return new ArrayList(loadString.Split(','));
        }
    }

    public static int GetLevelsUnlocked()
    {
        int scoreValue = PlayerPrefs.GetInt(GlobalConstants.KeyScoreValue, 0);
        if (scoreValue >= 150)
        {
            return 4;
        }
        else if (scoreValue >= 100)
        {
            return 3;
        }
        else if (scoreValue >= 50)
        {
            return 2;
        }
        else return 1;
    }
}