# Capstone AR

Repositori *source code* dari aplikasi media pembelajaran sains.

Aplikasi ini dikembangkan dengan menggunakan Unity Engine, dan Vuforia untuk *image detection*.

Model 3D dan gambar didapatkan dari:
- ![NASA Solar System](https://solarsystem.nasa.gov/resources/)
- ![Open ESA](https://open.esa.int/)
- SketchFab User: ![Tortillon](https://sketchfab.com/3d-models/67pchuryumovgerasimenko-593cdd9d19c44c93ac774c13392969b9), ![C. Yamahata](https://sketchfab.com/3d-models/moon-phases-226af19a53fc4cfeb623c7480857a7b6), ![Ipay](https://sketchfab.com/3d-models/earth-core-v2-6ba6e494d58c4465a45d7df85bffe97b)
